# ML.NET Labs 🤖

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/tonytins/mlnetlabs/HEAD?urlpath=lab) [![LICENSE](https://img.shields.io/github/license/tonytins/mlnetlabs?color=808080)](https://github.com/tonytins/dotnetlabs/blob/master/LICENSE)

ML.NET Labs is a research project into machine learning using Microsoft's ML.NET. The project is based on a variety of tutorials to get a firm understanding of the framework.

## Prerequisites

- [.NET](https://dotnet.microsoft.com/download) 5 or above
- [Visual Studio Code Insiders](https://code.visualstudio.com/insiders/)
- [.NET Interactive](https://github.com/dotnet/interactive/blob/main/README.md)
    - [VSCode Extension](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.dotnet-interactive-vscode) (does not require Jupyter)

## License
  
I license this project under the CC-BY 4.0 license and data under the Open Data Commons Open Database License - see the [LICENSE](LICENSE) in the root project and ``/notebooks/data`` directories, respectfully.

